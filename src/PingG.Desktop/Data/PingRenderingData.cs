using System.Net.NetworkInformation;

namespace PingG.Desktop.Data;

public class PingRenderingData
{
    public required string HostName { get; set; }

    public string StrokeStyle { get; set; } = "#fb7185";

    public required TimeSpan RecordingTime { get; set; }

    public PingStatistics Statistics { get; private set; } = new();

    private readonly Queue<RoundtripTimeWithTimestamp> _roundtripTimes = [];

    public void AddData(PingReply pingReply)
    {
        DateTime now = DateTime.Now;

        _roundtripTimes.Enqueue(new(now, pingReply.RoundtripTime, pingReply.Status));

        var end = now - RecordingTime;

        while (_roundtripTimes.Peek().Timestamp < end)
        {
            _roundtripTimes.Dequeue();
        }

        var rtts = _roundtripTimes.Select(e => e.RoundtripTime);
        var packetLossRate = (double)_roundtripTimes.Where(e => e.Status != IPStatus.Success).Count() / _roundtripTimes.Count;

        Statistics = new(pingReply.RoundtripTime, rtts.Min(), rtts.Max(), rtts.Average(), packetLossRate);
    }
}

public readonly record struct RoundtripTimeWithTimestamp(DateTime Timestamp, long RoundtripTime, IPStatus Status);

public readonly record struct PingStatistics(long Last, long Minimum, long Maximum, double Average, double PacketLossRate);
