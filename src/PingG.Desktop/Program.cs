using Photino.Blazor;

class Program
{
    [STAThread]
    static void Main(string[] args)
    {
        var builder = PhotinoBlazorAppBuilder.CreateDefault(args);

        builder.RootComponents.Add<PingG.Desktop.App>("#app");

        var app = builder.Build();

        app.MainWindow
            .SetUseOsDefaultLocation(true)
            .SetSize(1000, 600);

        app.Run();
    }
}
