using System.Diagnostics;
using System.Net.NetworkInformation;

using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using PingG.Desktop.Data;

namespace PingG.Desktop.Pages;

public partial class Home : ComponentBase, IAsyncDisposable
{
    private IJSObjectReference? _module;

    private async Task<IJSObjectReference> GetJSModuleAsync() => _module ??= await JS.InvokeAsync<IJSObjectReference>("import", "./Pages/Home.razor.js");

    private readonly List<PingRenderingData> _pingRenderingDatas = [];

    private readonly CancellationTokenSource _cancellationTokenSource = new();

    protected override void OnInitialized()
    {
        base.OnInitialized();

        _pingRenderingDatas.AddRange([
            new() { HostName = "1.1.1.1", StrokeStyle = "#fb7185", RecordingTime = default },
            new() { HostName = "GL-B1300", StrokeStyle = "#34d399", RecordingTime = default }
        ]);
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            var module = await GetJSModuleAsync();

            await module.InvokeVoidAsync("initDatasets", [_pingRenderingDatas.ToDictionary(e => e.HostName)]);

            await UpdateRecordingTimeAsync(TimeSpan.FromSeconds(30));

            var context = SynchronizationContext.Current;

            foreach (var data in _pingRenderingDatas)
            {
                _ = TimerUpdateAsync(data, _cancellationTokenSource.Token)
                    .ContinueWith(task => context?.Send(state =>
                    {
                        if (state is Exception ex)
                        {
                            throw ex;
                        }
                    },
                    task.Exception));
            }
        }
    }

    private async Task UpdateRecordingTimeAsync(TimeSpan recordingTime)
    {
        foreach (var renderingData in _pingRenderingDatas)
        {
            renderingData.RecordingTime = recordingTime;
        }

        var module = await GetJSModuleAsync();

        await module.InvokeVoidAsync("updateRecordingTime", recordingTime.TotalMilliseconds);
    }

    private async Task TimerUpdateAsync(PingRenderingData renderingData, CancellationToken cancellationToken)
    {
        using var timer = new PeriodicTimer(TimeSpan.FromMilliseconds(200));

        while (await timer.WaitForNextTickAsync(cancellationToken))
        {
            using var ping = new Ping();

            var hostName = renderingData.HostName;

            var reply = await ping.SendPingAsync(hostName, TimeSpan.FromSeconds(3), cancellationToken: cancellationToken);

            var module = await GetJSModuleAsync();

            await module.InvokeVoidAsync("update", cancellationToken,
                hostName,
                reply.RoundtripTime
            );

            renderingData.AddData(reply);

            StateHasChanged();
        }
    }

    async ValueTask IAsyncDisposable.DisposeAsync()
    {
        _cancellationTokenSource.Dispose();

        if (_module is not null)
        {
            await _module.DisposeAsync();
        }
    }
}
