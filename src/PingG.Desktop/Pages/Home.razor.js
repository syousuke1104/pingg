const datasets = {};

let _recordingTime = 30 * 1000;

const canvas = document.getElementById('canvas');

export function updateRecordingTime(recordingTime) {
    _recordingTime = recordingTime;
}

export function initDatasets(targets) {
    for (const key in targets) {
        datasets[key] = targets[key];
        datasets[key].data = [];
    }
}

export function update(id, roundtripTime) {

    const now = Date.now();

    const data = datasets[id].data;

    // 60秒 (MAX_DATA_BY_TIME) 以上前のデータを削除する
    const minTime = now - _recordingTime;
    const index = Math.max(data.findIndex(e => minTime < e.timestamp), 0);
    datasets[id].data = data.slice(index);

    // データを追加
    datasets[id].data.push({
        timestamp: now,
        roundtripTime: roundtripTime
    });
}

function draw() {

    const context = canvas.getContext("2d");

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.lineWidth = 3;
    context.lineJoin = "round";

    let max = Number.MIN_VALUE;
    let min = Number.MAX_VALUE;

    for (const property in datasets) {
        max = Math.max(...datasets[property].data.map(e => e.roundtripTime), max);
        min = Math.min(...datasets[property].data.map(e => e.roundtripTime), min);
    }

    const offsetX = 70;
    const offsetY = 2;

    // グラフ表示
    for (const property in datasets) {

        const values = datasets[property].data;

        context.strokeStyle = datasets[property].strokeStyle;
        context.beginPath();

        for (let i = 0; i < values.length; i++) {
            const p = (values[i].roundtripTime - min) / (max - min) * 0.9 + 0.05;
            const t = (Date.now() - values[i].timestamp) / _recordingTime;
            context.lineTo(offsetX + (canvas.width - offsetX) * (1 - t), (canvas.height - offsetY) * (1 - p));
        }

        context.stroke();
    }

    // グラフ枠表示
    context.lineWidth = 1;
    context.strokeStyle = "rgba(255, 255, 255, 0.5)";
    context.strokeRect(offsetX, 1, canvas.width - offsetX - 1, canvas.height - offsetY);

    // テキスト表示
    context.fillStyle = "white";
    context.font = "16px sans-serif";

    const maxStr = max === Number.MIN_VALUE ? "0 ms" : `${max} ms`;
    const minStr = min === Number.MAX_VALUE ? "0 ms" : `${min} ms`;

    const maxStrWidth = context.measureText(maxStr).width;
    const minStrWidth = context.measureText(minStr).width;

    context.fillText(maxStr, offsetX - maxStrWidth - 10, 26);
    context.fillText(minStr, offsetX - minStrWidth - 10, -10 + canvas.height - offsetY);
}

// Canvasの幅を親要素にフィットさせる関数
function fitCanvasToParent() {
    // 親要素の幅と高さを取得
    const width = canvas.parentElement.offsetWidth;
    const height = canvas.parentElement.offsetHeight;

    // Canvasのスタイルを設定
    canvas.style.width = width + 'px';
    canvas.style.height = height + 'px';

    // Canvasの描画領域のサイズを設定
    canvas.width = width;
    canvas.height = height;

    draw();
}

function animate() {
    draw();
    window.requestAnimationFrame(animate);
}

// Canvasのサイズを初期化
fitCanvasToParent();

// ウィンドウがリサイズされた際に、Canvasのサイズを更新
window.addEventListener('resize', fitCanvasToParent);

// Canvas描画
window.requestAnimationFrame(animate);
